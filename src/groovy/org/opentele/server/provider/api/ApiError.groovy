package org.opentele.server.provider.api

class ApiError {
    String field
    ErrorCode error
    String resource

    public ApiError(String resource, ErrorCode error) {
        this(resource, error, null)
    }

    public ApiError(String resource, ErrorCode error, String field) {

        this.resource = resource
        this.error = error
        this.field = field
    }
}
