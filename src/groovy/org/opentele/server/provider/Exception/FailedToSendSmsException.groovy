package org.opentele.server.provider.Exception

class FailedToSendSmsException extends RuntimeException {

    FailedToSendSmsException() {
        super()
    }

    FailedToSendSmsException(String s) {
        super(s)
    }

    FailedToSendSmsException(String s, Throwable throwable) {
        super(s, throwable)
    }

    FailedToSendSmsException(Throwable throwable) {
        super(throwable)
    }

    FailedToSendSmsException(String s, Throwable throwable, boolean b, boolean b1) {
        super(s, throwable, b, b1)
    }
}
