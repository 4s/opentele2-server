package org.opentele.server

import org.opentele.server.core.test.AbstractIntegrationSpec

class EncodeAsHTMLIntegrationSpec extends AbstractIntegrationSpec {

    def grailsApplication

    def "Test that we ensure HTML safe strings"() {

        when:
        grailsApplication != null

        then:
        assert grailsApplication.config.grails.views.default.codec == "html"
    }

}
