package org.opentele.server.provider

import org.opentele.server.core.exception.EntityNotFoundException
import org.opentele.server.model.Link
import org.opentele.server.model.LinksCategory
import org.opentele.server.model.PatientGroup
import org.springframework.transaction.interceptor.TransactionAspectSupport

class LinksService {

    def createCategory(String name, List links, List<Long> patientGroupIds) {
        def patientGroups = PatientGroup.getAll(patientGroupIds)

        def category = new LinksCategory(name: name)
        links.each {category.addToLinks(new Link(title: it.title, url: it.url))}
        patientGroups.each {category.addToPatientGroups(it)}

        def saved = category.save()
        if (saved) {
            return saved
        }
        return category
    }

    def updateCategory(long id, long expectedVersion, String name, List links, List<Long> patientGroupIds) {
        def toUpdate = LinksCategory.get(id)
        if (!toUpdate) {
            throw new EntityNotFoundException()
        }

        if (toUpdate.version > expectedVersion) {
            toUpdate.errors.rejectValue("version", "linksCategory.optimistic.locking.failure")
            return toUpdate
        }

        toUpdate.name = name
        updateLinks(toUpdate, links)
        updatePatientGroups(toUpdate, patientGroupIds)

        toUpdate.save()
        if (toUpdate.hasErrors()) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly()
        }

        toUpdate
    }

    private updateLinks(LinksCategory toUpdate, List links) {
        def oldLinks = []
        oldLinks += toUpdate.links
        oldLinks.each {
            toUpdate.removeFromLinks(it)
            it.delete()
        }

        links.each {
            toUpdate.addToLinks(new Link(title: it.title, url: it.url))
        }
    }

    private updatePatientGroups(LinksCategory toUpdate, List<Long> patientGroupIds) {
        toUpdate.patientGroups.clear()
        def groups = PatientGroup.getAll(patientGroupIds)
        groups.each {
            toUpdate.addToPatientGroups(it)
        }
    }

    def deleteCategory(long id) {
        def toDelete = LinksCategory.get(id)
        if (!toDelete) {
            throw new EntityNotFoundException()
        }

        toDelete.delete()
    }
}
