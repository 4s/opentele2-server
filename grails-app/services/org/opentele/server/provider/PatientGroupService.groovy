package org.opentele.server.provider

import grails.plugin.springsecurity.annotation.Secured
import org.opentele.server.model.Clinician2PatientGroup
import org.opentele.server.model.Patient
import org.opentele.server.model.Patient2PatientGroup
import org.opentele.server.model.PatientGroup
import org.opentele.server.model.StandardThresholdSet
import org.opentele.server.core.model.types.PermissionName
import org.springframework.dao.DataIntegrityViolationException
import org.springframework.transaction.annotation.Transactional
import org.springframework.transaction.interceptor.TransactionAspectSupport

class PatientGroupService {

    @Secured(PermissionName.PATIENT_GROUP_DELETE)
    @Transactional
    public boolean deletePatientGroup(PatientGroup patientGroup) {
        if (patientGroup == null) {
            throw new IllegalArgumentException("Cannot delete PatientGroup: " + patientGroup)
        }

        try {
            StandardThresholdSet st = patientGroup.standardThresholdSet

            st.patientGroup = null
            st.save(failOnError: true, flush: true)

            PatientGroup.executeUpdate("delete PatientGroup pg where pg = ?", [patientGroup])

            st.delete(failOnError: true, flush: true)

        } catch (DataIntegrityViolationException e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly()
            println e.getMessage()
            e.printStackTrace()

            return false
        }
        return true
    }

    @Secured(PermissionName.PATIENT_GROUP_READ)
    public List getPatientGroupReferences(PatientGroup patientGroup) {
        List nonEmptyReferences = []

        if (patientGroup.clinician2PatientGroups != null && !patientGroup.clinician2PatientGroups.isEmpty()) {
            nonEmptyReferences << Clinician2PatientGroup.getSimpleName()
        }

        if (patientGroup.patient2PatientGroups != null && !patientGroup.patient2PatientGroups.isEmpty()) {
            nonEmptyReferences << Patient2PatientGroup.getSimpleName()
        }

        if (patientGroup.patients != null && !patientGroup.patients.isEmpty()) {
            nonEmptyReferences << Patient.getSimpleName()
        }

        nonEmptyReferences
    }

}
