
<%@ page import="org.opentele.server.model.TextMessageTemplate" %>
<!doctype html>
<html>
<head>
    <meta name="layout" content="main">
    <title><g:message code="default.show.label" args="[g.message(code:'textMessageTemplate.label')]" /></title>
</head>
<body>

<div id="show-textMessageTemplates" class="content scaffold-show" role="main">
    <h1><g:message code="default.show.label" args="[g.message(code:'textMessageTemplate.label')]" /></h1>
    <g:if test="${flash.message}">
        <div class="message" role="status">${flash.message}</div>
    </g:if>
    <ol class="property-list textMessageTemplate">

        <g:if test="${template?.name}">
            <li class="fieldcontain">
                <span id="name-label" class="property-label"><g:message code="textMessageTemplate.name.label" /></span>
                <span class="property-value-long-text" aria-labelledby="name-label">${template?.name}</span>
            </li>
        </g:if>

        <g:if test="${template?.content}">
            <li class="fieldcontain">
                <span id="content-label" class="property-label"><g:message code="textMessageTemplate.content.label" /></span>
                <span class="property-value-long-text" aria-labelledby="content-label">${template?.content}</span>
            </li>
        </g:if>

    </ol>
    <g:form>
        <fieldset class="buttons">
            <g:hiddenField name="id" value="${template?.id}" />
            <g:link class="edit" action="edit" id="${template?.id}"><g:message code="default.button.edit.label" /></g:link>
            <g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label')}" onclick="return confirm('${message(code: 'textMessageTemplate.delete.message.confirm')}');" />
        </fieldset>
    </g:form>
</div>
</body>
</html>
