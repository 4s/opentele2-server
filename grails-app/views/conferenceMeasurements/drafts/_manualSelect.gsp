<g:encodeAs codec="raw">
    <div>
        ${title}: <g:select id="${name}-select" name="${name}-select" from="${options}" keys="${options}"/>
        <g:hiddenField id="${name}" name="${name}" value="${options[0]}"/>
    </div>
</g:encodeAs>