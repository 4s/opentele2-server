<tmpl:drafts/measurement measurement="${measurement}"
                         headline="${g.message(code:'conferenceMeasurement.bloodSugar.manual.title')}">

    <tmpl:drafts/manualValue
            name="bloodSugar"
            title="${g.message(code: 'conferenceMeasurement.bloodSugar.bloodSugar.title')}"
            value="${g.formatNumber(number: measurement.bloodSugar, format: '0.0')}">
        <g:message code="conferenceMeasurement.bloodSugar.bloodSugar.format"/>
    </tmpl:drafts/manualValue>

</tmpl:drafts/measurement>