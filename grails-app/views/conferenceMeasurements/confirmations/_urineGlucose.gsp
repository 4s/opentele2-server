<td><g:message code="conferenceMeasurement.urineGlucose.confirmation.title"/></td>
<td>
    <span>
        <div>
            <g:message code="conferenceMeasurement.urineGlucose.confirmation.urineGlucose"
                       args="[measurement.glucoseInUrine]"/>
        </div>
    </span>
</td>
