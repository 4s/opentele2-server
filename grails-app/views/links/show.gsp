
<%@ page import="org.opentele.server.model.TextMessageTemplate" %>
<!doctype html>
<html>
<head>
    <meta name="layout" content="main">
    <title><g:message code="default.show.label" args="[g.message(code:'linksCategory.list.label')]" /></title>
    <link rel="stylesheet" href="${resource(dir: 'css', file: 'links_style.css')}" type="text/css">
</head>
<body>


<div id="show-linksCategory" class="content scaffold-show" role="main">
    <h1><g:message code="default.show.label" args="[g.message(code:'linksCategory.label')]" /></h1>
    <g:if test="${flash.message}">
        <div class="message" role="status">${flash.message}</div>
    </g:if>
    <ol class="property-list linksCategory">

        <li class="fieldcontain">
            <span id="name-label" class="property-label"><g:message code="linksCategory.name.label" /></span>
            <span class="property-value-long-text" aria-labelledby="name-label">${category.name}</span>
        </li>
        <li class="fieldcontain">
            <span id="links-label" class="property-label"><g:message code="linksCategory.link.label" /></span>
            <table id="showLinksTable">
                <thead>
                <tr>
                    <th><g:message code="linksCategory.link.title.label"/></th>
                    <th><g:message code="linksCategory.link.url.label"/></th>
                </tr>
                </thead>
                <tbody>
                    <g:each in="${category.links}" var="link">
                        <tr>
                            <td>${link.title}</td>
                            <td>${link.url}</td>
                        </tr>
                    </g:each>
                </tbody>
            </table>

        </li>
        <li class="fieldcontain">
            <span id="patient-groups-label" class="property-label"><g:message code="linksCategory.patientgroup.label" /></span>
            <span class="property-value-long-text" aria-labelledby="patient-groups-label">
                <g:each in="${category.patientGroups}" var="group">
                    ${group.name}<br />
                </g:each>
            </span>
        </li>

    </ol>
    <g:form>
        <fieldset class="buttons">
            <g:hiddenField name="id" value="${category?.id}" />
            <g:link class="edit" action="edit" id="${category?.id}"><g:message code="default.button.edit.label" /></g:link>
            <g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label')}" onclick="return confirm('${message(code: 'linksCategory.delete.message.confirm')}');" />
        </fieldset>
    </g:form>
</div>
</body>
</html>
