<%@ page import="org.opentele.server.model.Clinician" %>
<!doctype html>
<html>
<head>
    <meta name="layout" content="main">
    <g:set var="entityName"
           value="${message(code: 'default.user.label', default: 'User')}"/>
    <title><g:message code="default.list.label" args="[entityName]"/></title>
</head>

<body>

<div id="list-clinician" class="content scaffold-list" role="main">
    <h1>
        <g:message code="default.list.label" args="[entityName]"/>
    </h1>
    <g:if test="${flash.message}">
        <div class="message" role="status">
            ${flash.message}
        </div>
    </g:if>

    <g:form action="list" method="GET">
        <div class="content fieldcontain">
            <g:textField name="queryText" id="clinician-search-query" value="${queryText}" maxlength="64" class="property-value"
                         data-tooltip="${message(code: 'tooltip.clinician.search')}"/>
            <div class="buttons search-button">
                <g:submitButton name="search" id="clinician-search-button" class="search"
                                value="${message(code: 'clinician.search.button.label')}"/>
            </div>
        </div>

        <table>
            <thead>
            <tr>
                <g:sortableColumn property="firstName" params='[queryText: "${queryText}"]'
                                  title="${message(code: 'clinician.firstName.label')}"/>
                <g:sortableColumn property="lastName" params='[queryText: "${queryText}"]'
                                  title="${message(code: 'clinician.lastName.label')}"/>
                <g:sortableColumn property="phone" params='[queryText: "${queryText}"]'
                                  title="${message(code: 'clinician.phone.label')}"/>
                <g:sortableColumn property="mobilePhone" params='[queryText: "${queryText}"]'
                                  title="${message(code: 'clinician.mobilePhone.label')}"/>
                <g:sortableColumn property="email" params='[queryText: "${queryText}"]'
                                  title="${message(code: 'clinician.email.label')}"/>
                <g:sortableColumn property="username" params='[queryText: "${queryText}"]'
                                  title="${message(code: 'clinician.username.label')}"/>
                <g:if test="${Boolean.valueOf(grailsApplication.config.video.enabled)}">
                    <g:sortableColumn property="videoUser" params='[queryText: "${queryText}"]'
                                      title="${message(code: 'clinician.video_user.label')}"/>
                </g:if>
            </tr>
            </thead>
            <tbody>
            <g:each in="${clinicianInstanceList}" status="i"
                    var="clinicianInstance">
                <tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
                    <td>
                        <g:link action="show" id="${clinicianInstance.id}">
                            ${fieldValue(bean: clinicianInstance, field: "firstName")}
                        </g:link>
                    </td>
                    <td>
                        ${fieldValue(bean: clinicianInstance, field: "lastName")}
                    </td>
                    <td>
                        ${fieldValue(bean: clinicianInstance, field: "phone")}
                    </td>
                    <td>
                        ${fieldValue(bean: clinicianInstance, field: "mobilePhone")}
                    </td>
                    <td>
                        ${fieldValue(bean: clinicianInstance, field: "email")}
                    </td>
                    <td>
                        <g:if test="${clinicianInstance?.user?.username}">
                            ${fieldValue(bean: clinicianInstance, field: "user.username")}
                        </g:if>
                    </td>
                    <g:if test="${Boolean.valueOf(grailsApplication.config.video.enabled)}">
                        <td>
                            ${fieldValue(bean: clinicianInstance, field: "videoUser")}
                        </td>
                    </g:if>
                </tr>
            </g:each>
            </tbody>
        </table>
    </g:form>

    <div class="pagination">
        <g:paginate total="${clinicianInstanceTotal}" params='[queryText: "${queryText}"]'/>
    </div>
</div>
<fieldset class="buttons">
    <g:link class="create" action="create">
        <g:message code="default.new.person.label" args="[entityName]"/>
    </g:link>
</fieldset>
</body>
</html>
