
<%@ page import="org.opentele.server.model.TextMessageTemplate" %>
<!doctype html>
<html>
<head>
    <meta name="layout" content="main">
    <title><g:message code="default.show.label" args="[g.message(code:'textMessages.label')]" /></title>
</head>
<body>

<div id="show-send-textMessages" class="content scaffold-show" role="main">
    <h1><g:message code="textMessages.label"/></h1>
    <g:if test="${flash.message}">
        <div class="message" role="status">${flash.message}</div>
    </g:if>
    <g:form>
        <fieldset class="form">
            <div class="fieldcontain">
                <label for="selected">
                    <g:message code="textMessages.name.label" default="Message:" />
                </label>
                <g:select name="selected" from="${templates}" optionKey="content" optionValue="${{it?.shortName()}}"
                          noSelection="${['':message(code:'textMessages.default.entry')]}"
                          onchange="selectedTemplateChanged(this.value)" />
            </div>
            <div class="fieldcontain">
                <span id="content-label" class="property-label"><g:message code="textMessages.content.label" /></span>
                <span id="message-content" class="property-value-long-text" aria-labelledby="content-label"></span>
            </div>
        </fieldset>
        <fieldset class="buttons">
            <g:hiddenField name="patientId" value="${patientId}" />
            <g:actionSubmit id="send" class="send" action="submit" value="${message(code: 'textMessages.send.label')}" />
        </fieldset>
    </g:form>
</div>

<g:javascript>
    function selectedTemplateChanged(selected) {
        $("span#message-content").text(selected)
        checkSendEnabled()
    }

    function checkSendEnabled() {
        var selected = $('select[name="selected"]').val()
        if (selected === '' || selected === null) {
            $('input#send').attr('disabled', 'disabled');

        } else {
            $('input#send').removeAttr('disabled');
        }
    }

    checkSendEnabled()
</g:javascript>
</body>
</html>
